<?php

require 'ProjectNameNumber.php';

use PHPUnit\Framework\TestCase;

class ProjectNameNumbertTest extends TestCase
{
    /**
     * Class for testing project name number than creating project,.
     */

    public function testProjectNameNumberIsInteger()
    {
        $number = new ProjectNameNumber();
        $this->assertTrue($number->checkIfIsInteger('20')); // returns true is is integer
        $this->assertFalse($number->checkIfIsInteger('A2')); //returns false value because not integer
    }
    public function testProjectNameNumberIsLargerThan10000()
    {
        $number = new ProjectNameNumber();
        $this->assertTrue($number->checkIfLargerThan10000('20')); // checking if 20 is larger than 10000 - returns true
        $this->assertFalse($number->checkIfLargerThan10000('25000')); // checking if 25000 is larger than 10000 - returns false
    }
    public function testProjectNameNumberIsLowerThan1()
    {
        $number = new ProjectNameNumber();
        $this->assertTrue($number->checkIsLowerThan1('20')); // checking if 20 is lower than 1 - returns true
        $this->assertFalse($number->checkIsLowerThan1('-30')); // checking if -30 is lower than 1 - returns false
    }
}
