<?php

class Date
{
    public function checkDateFormat($date)
    {
        if (strlen($date) >= 10) {
            return true;
        } else {
            return false;
        }
    }
    public function checkDateInterval($date)
    {
        $dateToInt = strtotime($date);
        $dateFrom = strtotime('1980-01-01 00:00:00');
        $dateTo = strtotime('2050-01-01 00:00:00');
        if ($dateToInt > $dateFrom && $dateToInt < $dateTo) {
            return true;
        } else {
            return false;
        }
    }
    public function checkDateIsInteger($date)
    {
        $res = str_replace(array(
            '-',
            ':',
            ' '
        ), '', $date);

        if (is_numeric($res)) {
            return true;
        } else {
            return false;
        }
    }
}
